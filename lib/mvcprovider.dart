library mvcprovider;

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

///
/// The [MVC_Module] class which is the link between MVC entities
/// and optionally declare new Provider classes with property "providers"
///
/// Sample class :
/// ```dart
/// class MyCounterModule extends MVC_Module<MyCounterModel, MyCounterView, MyCounterCtrl> {
///
///   final MyCounterModel model = MyCounterModel();
///   final MyCounterView view = MyCounterView();
///   final MyCounterCtrl ctrl = MyCounterCtrl();
///
///   // A sample list of services/providers declared at module level
///   final List<SingleChildWidget> providers = [
///     MyThemeService().create, // New V2 syntax (if MyThemeService extends MVC_Notifier<MyThemeService>)
///     MyRouterService().create, // New V2 syntax
///     MyAuthenticationService().create, // New V2 syntax
///     ChangeNotifierProvider(create: (context) => MyUserProfileService()) // Original Provider syntax
///   ];
/// }
/// ```
///
class MVC_Module<M extends MVC_Model, V extends Widget,
    C extends MVC_Controller> extends StatelessWidget {
  final M model;
  final V view;
  final C ctrl;
  final List<SingleChildWidget> providers;

  MVC_Module({this.model, @required this.view, this.ctrl, this.providers});

  bool get isProviders => providers != null && providers.length > 0;
  bool get isModel => model != null;
  bool get isCtrl => ctrl != null;

  @override
  Widget build(BuildContext context) {
    return isProviders || isModel || isCtrl
        ? MultiProvider(providers: [
            ...(isProviders ? providers : []),
            ...(isModel
                ? [
                    ChangeNotifierProvider<M>.value(
                        value: model.create(context, ctrl))
                  ]
                : []),
            ...(isCtrl
                ? [
                    ChangeNotifierProvider<C>.value(
                        value: ctrl.create(context, model))
                  ]
                : [])
          ], builder: (context, child) => view)
        : view;
  }
}

///
/// The [MVC_Model] class which will contain/manipulate data
/// and optionally trigger a view rebuild/refresh by calling "notifyListeners()"
///
/// Sample class :
/// ```dart
/// class MyCounterModel extends MVC_Model<MyCounterCtrl> {
///
///   int _count = 0;
///   set count(int value) {
///     _count = value;
///     notifyListeners();
///   }
///   int get count => _count;
/// }
/// ```
///
class MVC_Model<C> extends ChangeNotifier {
  ///
  /// The [MVC_Model]'s property which will store the current context
  ///
  BuildContext context;

  ///
  /// The [MVC_Model]'s property which will store the controller
  ///
  C ctrl;

  ///
  /// The [MVC_Model]'s method called before first rendering
  /// it shouldn't be overrided but here is a...
  ///
  /// ...Sample override >_< :
  /// ```dart
  ///   @override
  ///   MyCounterModel create (BuildContext context, MyCounterCtrl ctrl) {
  ///     // You probably should override init() method instead
  ///     return super.create(context, ctrl);
  ///   }
  /// ```
  ///
  MVC_Model create(BuildContext context, C ctrl) {
    this.context = context;
    this.ctrl = ctrl;
    this.init();
    return this;
  }

  ///
  /// The [MVC_Model]'s method called in create() method
  /// where you should make your http requests and/or init values
  ///
  /// Sample override :
  /// ```dart
  ///   @override
  ///   void init() {
  ///     // Retrieve some data and/or init some ctrl/model variables
  ///   }
  /// ```
  ///
  void init() {}

  ///
  /// The [MVC_Model]'s method called every rendering
  ///
  /// Sample override :
  /// ```dart
  ///   @override
  ///   MyCounterModel update (BuildContext context) {
  ///     // Update AND/OR check some things every frame
  ///     return super.update(context);
  ///   }
  /// ```
  ///
  MVC_Model update(BuildContext context) {
    this.context = context;
    return this;
  }
}

///
/// The [MVC_Controller] class which will handle user's actions
/// and optionally trigger a view rebuild/refresh by calling "notifyListeners()"
///
/// Sample class :
/// ```dart
/// class MyCounterCtrl extends MVC_Controller<MyCounterModel> {
///
///   void increment() {
///     model.count++;
///   }
/// }
/// ```
///
class MVC_Controller<M> extends ChangeNotifier {
  ///
  /// The [MVC_Controller]'s property which will store the current context
  ///
  BuildContext context;

  ///
  /// The [MVC_Controller]'s property which will store the model
  ///
  M model;

  ///
  /// The [MVC_Controller]'s method called before first rendering
  /// it shouldn't be overrided but here is a...
  ///
  /// ...Sample override >_< :
  /// ```dart
  ///   @override
  ///   MyCounterCtrl create (BuildContext context, MyCounterModel model) {
  ///     // You probably should override init() method instead
  ///     return super.create(context, model);
  ///   }
  /// ```
  ///
  MVC_Controller create(BuildContext context, M model) {
    this.context = context;
    this.model = model;
    this.init();
    return this;
  }

  ///
  /// The [MVC_Controller]'s method called in create() method
  /// where you should make your http requests and/or init values
  ///
  /// Sample override :
  /// ```dart
  ///   @override
  ///   void init() {
  ///     // Retrieve some data and/or init some ctrl/model variables
  ///   }
  /// ```
  ///
  void init() {}

  ///
  /// The [MVC_Controller]'s method called every rendering
  ///
  /// Sample override :
  /// ```dart
  ///   @override
  ///   MyCounterCtrl update (BuildContext context) {
  ///     // Update AND/OR check some things every frame
  ///     return super.update(context);
  ///   }
  /// ```
  ///
  MVC_Controller update(BuildContext context) {
    this.context = context;
    return this;
  }
}

///
/// A private class which consist in a type definition
/// in order to keep view's properties immutable
///
class _MC<M, C> {
  ///
  /// The _MC's property which will store the model
  ///
  M model;

  ///
  /// The _MC's property which will store the controller
  ///
  C ctrl;
}

///
/// The [MVC_View] mixin which will display model data and link
/// controller's methods to user's actions
///
/// Sample use :
/// ```dart
/// class MyCounterView extends StatelessWidget with MVC_View<MyCounterModel, MyCounterCtrl> {
///
///   @override
///   Widget build(BuildContext context) {
///     listen(context);
///
///     return Column(
///         mainAxisAlignment: MainAxisAlignment.center,
///         children: [
///           Text("Counter : ${model.count}"),
///           RaisedButton(
///             child: Text("Increment"),
///             onPressed: ctrl.increment,
///           )
///         ]
///     );
///   }
/// }
/// ```
///
mixin MVC_View<M extends MVC_Model, C extends MVC_Controller> {
  ///
  /// The [MVC_View]'s property which will store model AND/OR controller references
  ///
  final _MC<MVC_Model, C> _ = _MC<MVC_Model, C>();

  ///
  /// The [MVC_View]'s getter for the model
  ///
  M get model => _.model;

  ///
  /// The [MVC_View]'s getter for the controller
  ///
  C get ctrl => _.ctrl;

  ///
  /// The [MVC_View]'s method to call at the beginning of the widget's build method
  /// in order to listen for controller OR model changes (when they call "notifyListeners()")
  ///
  /// Sample use :
  /// ```dart
  ///   @override
  ///   Widget build(BuildContext context) {
  ///     listen(context);
  ///     // ...Your widget content
  ///   }
  /// ```
  ///
  void listen(BuildContext context) {
    listenModel(context);
    listenCtrl(context);
  }

  ///
  /// The [MVC_View]'s method to call at the beginning of the widget's build method
  /// in order to listen for model changes (when it call "notifyListeners()")
  ///
  /// Sample use :
  /// ```dart
  ///   @override
  ///   Widget build(BuildContext context) {
  ///     listenModel(context);
  ///     // ...Your widget content
  ///   }
  /// ```
  ///
  void listenModel(BuildContext context) {
    _.model = Provider.of<M>(context).update(context);
  }

  ///
  /// The [MVC_View]'s method to call at the beginning of the widget's build method
  /// in order to listen for controller changes (when it call "notifyListeners()")
  ///
  /// Sample use :
  /// ```dart
  ///   @override
  ///   Widget build(BuildContext context) {
  ///     listenCtrl(context);
  ///     // ...Your widget content
  ///   }
  /// ```
  ///
  void listenCtrl(BuildContext context) {
    _.ctrl = Provider.of<C>(context).update(context);
  }
}

///
/// V2 Sugar syntax to declare a [ChangeNotifierProvider]
/// When you want to share an instance of a class able to trigger rebuilds when needed
///
/// Sample declaration (in a [MVC_MODULE.providers] property OR in a [MultiProvider] Widget) :
/// ```dart
/// [...
///   MyThemeService().create,
/// ...]
/// ```
///
/// Sample use :
/// ```dart
/// ThemeMode currentThemeMode = MyThemeService(context).listen.mode;
///   AND / OR
/// MyThemeService(context).get.toggle();
/// ```
///
/// Sample class :
/// ```dart
/// class MyThemeService extends MVC_Notifier<MyThemeService> {
///
///   MyThemeService([context]) : super(context); // The constructor which call the inherited "super" constructor
///
///   ThemeMode mode = ThemeMode.system; // The property where the theme mode is stored
///
///   // The method which will switch between light OR dark themes
///   void toggle() {
///     mode = (mode == ThemeMode.system || mode == ThemeMode.light) ? ThemeMode.dark : ThemeMode.light;
///     notifyListeners();
///   }
/// }
/// ```
///
abstract class MVC_Notifier<T extends ChangeNotifier> extends ChangeNotifier {
  ///
  /// The [MVC_Notifier]'s property which will store the current context
  ///
  BuildContext context;

  ///
  /// The [MVC_Notifier]'s constructor which will return an [MVC_Notifier] instance with given context
  ///
  MVC_Notifier([this.context]);

  ///
  /// The [MVC_Notifier]'s method which will update context of current [MVC_Notifier] instance
  ///
  T update(BuildContext context) {
    this.context = context;
    return this as T;
  }

  ///
  /// A [MVC_Notifier]'s getter which will return the [MVC_Notifier] instance declared at an higher widget level
  /// it should be used in views when you need to rebuild them when "notifyListeners()" is called
  ///
  T get listen => (Provider.of<T>(context) as MVC_Notifier).update(context);

  ///
  /// A [MVC_Notifier]'s getter which will return the [MVC_Notifier] instance declared at higher level
  /// it should be used anytime else
  ///
  T get get =>
      (Provider.of<T>(context, listen: false) as MVC_Notifier).update(context);

  ///
  /// A [MVC_Notifier]'s getter which will return the [ChangeNotifierProvider] instance which will
  /// init the [MVC_Notifier]'s instance which will be used at a lower widget level
  ///
  ChangeNotifierProvider<T> get create =>
      ChangeNotifierProvider<T>(create: update);
}

///
/// V2 Sugar syntax to declare a [Provider]
/// When you simply need to share an instance of a class
///
/// Sample declaration (in a [MVC_MODULE.providers] property OR in a [MultiProvider] Widget) :
/// ```dart
/// [...
///   MyRouterService().create,
/// ...]
/// ```
///
/// Sample use :
/// ```dart
/// MyRouterService(context).get.pushNamed('/home');
/// ```
///
/// Sample class :
/// ```dart
/// class MyRouterService extends MVC_Provider<MyRouterService> {
///
///   MyRouterService([context]) : super(context); // The constructor which call the inherited "super" constructor
///
///   // A list of functions associated to a list of route names
///   // In this case they prevent to navigate to some routes when AuthenticationService's property "isLogged" is not true
///   Map<String, Future<bool> Function(BuildContext)> _guards = {
///     "/home": (BuildContext context) => MyAuthenticationService(context).get.isLogged, // V2 syntax (if MyAuthenticationService extends MVC_Provider<MyAuthenticationService>)
///     "/list": (BuildContext context) => Provider.of<MyAuthenticationService>(context, listen: false).isLogged, // Original Provider syntax
///   };
///
///   // The method which will navigate to a new route only if the associated guard return true
///   Future<dynamic> pushNamed(BuildContext context, String route) async {
///     if (!_guards.containsKey(route) || await _guards[route](context)) {
///       return Navigator.pushNamed(context, route);
///     }
///     else return false;
///   }
///
/// }
/// ```
///
abstract class MVC_Provider<T> {
  ///
  /// The [MVC_Provider]'s property which will store the current context
  ///
  BuildContext context;

  ///
  /// The [MVC_Provider]'s constructor which will return an [MVC_Provider] instance with given context
  ///
  MVC_Provider([this.context]);

  ///
  /// The [MVC_Provider]'s method which will update context of current [MVC_Provider] instance
  ///
  T update(BuildContext context) {
    this.context = context;
    return this as T;
  }

  ///
  /// The [MVC_Provider]'s getter which will return the [MVC_Provider] instance declared at higher level
  ///
  T get get =>
      (Provider.of<T>(context, listen: false) as MVC_Provider).update(context);

  ///
  /// A [MVC_Provider]'s getter which will return the [Provider] instance which will
  /// init the [MVC_Provider]'s instance which will be used at a lower widget level
  ///
  Provider<T> get create => Provider<T>(create: update);
}

///
/// V2 Sugar syntax to declare a [ProxyProvider]
/// When you need to share an instance's method or property with a given type
///
/// Sample declaration (in a [MVC_MODULE.providers] property OR in a [MultiProvider] Widget) :
/// ```dart
/// [...
///   MyUserProfileService().create,
/// ...]
/// ```
///
/// Sample use :
/// ```dart
/// String loggedUserName = MyUserProfileService(context).get["name"];
/// ```
///
/// Sample class :
/// ```dart
/// class MyUserProfileService extends MVC_ProxyProvider<MyUserProfileService, Map<String, dynamic>> {
///
///   MyUserProfileService([context]) : super(context); // The constructor which call the inherited "super" constructor
///
///   @override
///   Map<String, dynamic> builder(BuildContext context) {
///     return MyAuthenticationService(context).get.loggedUser;
///   }
/// }
/// ```
///
abstract class MVC_ProxyProvider<T, R> {
  ///
  /// The [MVC_ProxyProvider]'s property which will store the current context
  ///
  BuildContext context;

  ///
  /// The [MVC_ProxyProvider]'s constructor which will return an [MVC_ProxyProvider] instance with given context
  ///
  MVC_ProxyProvider([this.context]);

  ///
  /// The [MVC_ProxyProvider]'s method which will update context of current [MVC_ProxyProvider] instance
  ///
  T update(BuildContext context) {
    this.context = context;
    return this as T;
  }

  ///
  /// The [MVC_ProxyProvider]'s method which should be overrided in order to return something with the given [R] type
  ///
  R builder(BuildContext context);

  ///
  /// The [MVC_ProxyProvider]'s getter which will return something with the given [R] type provided by the [MVC_ProxyProvider] instance declared at higher level
  ///
  R get get => (Provider.of<T>(context, listen: false) as MVC_ProxyProvider)
      .update(context)
      .builder(context);

  ///
  /// A [MVC_ProxyProvider]'s getter which will return the [MultiProvider] instance which will
  /// init the [MVC_ProxyProvider]'s instance which will be used at a lower widget level
  ///
  SingleChildWidget get create => MultiProvider(
        providers: [
          Provider(create: update),
          ProxyProvider<T, R>(
            create: builder,
            update: (_context, T value, R previous) {
              return (value as MVC_ProxyProvider).builder(context);
            },
          ),
        ],
      );
}
