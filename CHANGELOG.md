
## [1.0.0] 03-14-2020

First release

## [1.0.1] 03-14-2020

Fixed dart warnings and added an example folder with an example :)

## [1.0.2] 03-14-2020

Ran flutter lib/mvcprovider.dart

## [1.0.3] 03-14-2020

Replaced module's root widget Material by a widget Container

## [1.0.4] 03-15-2020

Removed View's "StatelessWidget extends" in order to use it as a mixin
and so permit to use it with StatefulWidgets if needed

## [1.0.5] 06-10-2020

Updated sample doc
Added getters for model and ctrl properties in the View mixin
in order use it while extending from a StatelessWidget without
the "immutable class with non final fields" warning

## [1.0.6] 06-30-2020

Little refacto
And added ability to add declare new providers at module level with "providers" property
in addition to the "dependencies"

## [1.0.7] 07-16-2020

Added overridable method onReady to the Controller in order to retrieve or init some data
or just to do all you need to do once just after the first rendering of your view.

## [1.0.8] 08-12-2020

Removed "refresh()" method in the model and controller in order use the original provider
method "notifyListeners()" to keep your syntax compatible with the original flutter code.
Spreading error syntax has been removed.
Readme and example has been updated.

## [2.0.0] 10-06-2020

[Breaking changes]
Please rename *Module*, *Model*, *Controller* & *View*
with ***MVC_Module***, ***MVC_Model***, ***MVC_Controller*** & ***MVC_View***.

Please remove ***MVC_Module***'s property "*dependencies*". We don't need it anymore.

Now your services/providers should inherit from ***MVC_Provider***, ***MVC_Notifier*** or ***MVC_ProxyProvider***

To access dependencies inheriting from classes  ***MVC_Provider***, ***MVC_Notifier*** or ***MVC_ProxyProvider***
Please use: **MyClassName(context).get** instead of *$[MyClassName]* or *Provider.of<MyClassName>(context, listen: false)*.

Please replace ***MVC_Controller***'s methods *init()* & *onRefresh()* with **create()** & **update()**.
And note that *create()* &*update()* methods are now available in ***MVC_Model*** :)

Please remove ***MVC_Controller***'s method onReady(). We don't need it anymore. Please retrieve data or init
some things in the **create()** method by calling some HTTP request or retrieving dependencies.

Please replace your old provider declaration: *ChangeNotifierProvider(create: (context) => MyClassName());*
with the new **MyClassName().create;**


## [2.0.1] 10-09-2020

***MVC_Controller***'s methods **init()** is back ! Because when overriding *create()* you have to call
*super.create()* at start if you want to access *model* or *BuildContext* inside.

This is why the **init()** method is called at the end of *create()* so by overriding it you will access *model* or
*BuildContext* easily in order to retrieve data or init some things.

