import 'package:flutter/material.dart';
import 'package:mvcprovider/mvcprovider.dart';
import 'my.counter.ctrl.dart';
import 'my.counter.model.dart';

class MyCounterView extends StatelessWidget
    with MVC_View<MyCounterModel, MyCounterCtrl> {
  @override
  Widget build(BuildContext context, [Widget child]) {
    listen(context);

    return Column(mainAxisAlignment: MainAxisAlignment.center, children: [
      Text("Counter : ${model.count}"),
      RaisedButton(child: Text("Increment"), onPressed: ctrl.increment)
    ]);
  }
}
