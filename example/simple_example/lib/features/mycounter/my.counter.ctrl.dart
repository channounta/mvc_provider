import 'package:mvcprovider/mvcprovider.dart';
import 'my.counter.model.dart';

class MyCounterCtrl extends MVC_Controller<MyCounterModel> {
  void increment() {
    model.count++;
  }
}
